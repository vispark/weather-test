# Weather test app

## Setup

```
npm i
```
Also you need to create `.env` file. Example:

```
NODE_ENV=development

PROTOCOL=http
HOST=localhost
PORT=3000

OPENWEATHERMAP_API_KEY=your_api_key
```
 
 Finally
 
 ```
 npm start
 ```