const {PRODUCTION, MODE} = require('./environment.config');

const presets = [
  ['@babel/env', {
    debug: !PRODUCTION,
    loose: true,
    modules: MODE === 'test' ? void(0): false,
    useBuiltIns: 'usage',
    forceAllTransforms: PRODUCTION,
  }],

  ['@babel/preset-react', {
    development: !PRODUCTION,
  }],
];

const plugins = [
  '@babel/plugin-syntax-dynamic-import',
  '@babel/plugin-syntax-import-meta',
  '@babel/plugin-proposal-json-strings',

  ['babel-plugin-emotion', {
    hoist: PRODUCTION,
    autoLabel: !PRODUCTION,
    sourceMap: !PRODUCTION,
  }],

  ['@babel/plugin-proposal-decorators', { 
    legacy: true,
  }],

  ['@babel/plugin-proposal-class-properties', {
    loose: true,
  }],

  ['@babel/plugin-transform-runtime', {
      corejs: 2,
      helpers: true,
      regenerator: true,
      useESModules: false,
    },
  ],

  !PRODUCTION
    ? 'react-hot-loader/babel'
    : void(0),

].filter(Boolean);

module.exports = {
  presets,
  plugins,
};
