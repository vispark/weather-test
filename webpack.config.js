const ENVIRONMENT = require('./environment.config');

const path = require('path');

/* Third party plugins */
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

/* Webpack's plugins */
const DefinePlugin = require('webpack/lib/DefinePlugin');
const HashedModuleIdsPlugin = require('webpack/lib/HashedModuleIdsPlugin');
const HotModuleReplacementPlugin = require('webpack/lib/HotModuleReplacementPlugin');
const NamedModulesPlugin = require('webpack/lib/NamedModulesPlugin');
const OccurrenceOrderPlugin = require('webpack/lib/optimize/OccurrenceOrderPlugin');

const {
  MODE,
  PRODUCTION,
  HOST,
  PORT,
  PUBLIC_PATH,
} = ENVIRONMENT;

const SRC_PATH = path.resolve(__dirname, 'src');


/* webpack.entry */

const entry = {
  main: ['./src/application/index.js'],
};

!PRODUCTION
  ? entry.main.unshift(
    'react-hot-loader/patch',
    `webpack-dev-server/client?${PUBLIC_PATH}`,
    'webpack/hot/only-dev-server',
  )
  : void(0);


/* webpack.output */

const output = {
  path: path.resolve(__dirname, 'dist'),
  filename: 'assets/js/[name].[hash].js',
  chunkFilename: 'assets/js/[name].[chunkhash].js',
  publicPath: `${PUBLIC_PATH}/`,
};


/* webpack.devtool */

const devtool = PRODUCTION
  ? ''
  : 'cheap-module-source-map';


/* webpack.resolve */

const resolve = {
  extensions: ['.js', '.json'],
  modules: ['node_modules'],
  alias: {
    Assets: path.resolve(SRC_PATH, 'assets'),
    Components: path.resolve(SRC_PATH, 'application/components'),
    Core: path.resolve(SRC_PATH, 'application/core'),
    Theme: path.resolve(SRC_PATH, 'application/theme'),
  },
};


/* webpack.module */

const rules = {
  common: {
    parser: {
      requireEnsure: false,
    },
  },

  js: {
    test: /\.js$/,
    loader: 'babel-loader',
    include: path.resolve(SRC_PATH, 'application'),
    exclude: [/[/\\\\]node_modules[/\\\\]/],
  },

  css: {
    test: /\.css$/,
    use: [
      MiniCssExtractPlugin.loader,
      {
        loader: 'css-loader',
        options: {
          importLoaders: 1,
          sourceMap: true,
        },
      },
      {
        loader: 'postcss-loader',
        options: {
          // Necessary for external CSS imports to work
          // https://github.com/facebook/create-react-app/issues/2677
          ident: 'postcss',
          plugins: () => [
            require('postcss-flexbugs-fixes'),
            require('autoprefixer')({
              flexbox: 'no-2009',
            }),
          ],
          sourceMap: true,
        }
      },
    ],
  },

  fonts: {
    test: [/\.woff$/, /\.woff2$/, /\.eot$/, /\.ttf$/],
    loader: 'url-loader',
    options: {
      limit: 10000,
      name: 'assets/fonts/[name].[hash:8].[ext]',
    },
  },

  images: {
    test: [/\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.png$/, /\.svg$/],
    loader: 'url-loader',
    options: {
      limit: 10000,
      name: 'assets/images/[name].[hash:8].[ext]',
    },
  },
};

const modules = {
  strictExportPresence: true,
  rules: Object.values(rules),
};


/* webpack.dev-server */

const server = {
  compress: true,
  clientLogLevel: 'warning',
  historyApiFallback: true,
  hot: true,
  host: HOST,
  port: PORT,
  publicPath: PUBLIC_PATH,
  stats: {
    cached: true,
    cachedAssets: true,
    children: false,
    chunks: false,
    chunkModules: false,
    colors: true,
    modules: false,
    performance: false,
    hash: false,
    reasons: true,
    timings: true,
    version: false,
  }
};


/* webpack.plugins */

const plugins = [
  new CopyWebpackPlugin([{
    from: './src/assets',
    to: './assets',
    ignore: [/\.(js|jsx|mjs)$/, /\.html$/, /\.json$/, '.gitkeep'],
  }]),

  new DefinePlugin({
    'process.env': Object.entries(ENVIRONMENT)
      .reduce((accum, [key, value]) => ({
        ...accum,
        [key]: JSON.stringify(value),
      }), {}),
  }),
  new HashedModuleIdsPlugin(),

  new HTMLWebpackPlugin({
    title: 'Client | Best Practices Boilerplate',
    favicon: path.resolve(SRC_PATH, 'favicon.ico'),
    filename: 'index.html',
    template: path.resolve(SRC_PATH, 'index.html'),
    inject: true,
    meta: {
      description: 'Client Best Practices Boilerplate',
    },
    minify: {
      collapseWhitespace: true,
      processConditionalComments: true,
      keepClosingSlash: true,
      minifyCSS: true,
      minifyJS: true,
      minifyURLs: true,
      removeComments: true,
      removeRedundantAttributes: true,
      removeEmptyAttributes: true,
      removeStyleLinkTypeAttributes: true,
      useShortDoctype: true,
    },
    chunksSortMode: 'dependency',
  }),

  new ManifestPlugin({
    fileName: 'asset-manifest.json',
    publicPath: `${PUBLIC_PATH}/`,
  }),

  new MiniCssExtractPlugin({
    // Options similar to the same options in webpackOptions.output
    // both options are optional
    filename: 'assets/css/[name].[contenthash:8].css',
    chunkFilename: 'assets/css/[name].[contenthash:8].chunk.css',
  }),

  new NamedModulesPlugin(),

  new OccurrenceOrderPlugin(true),

  new SWPrecacheWebpackPlugin({
    // By default, a cache-busting query parameter is appended to requests
    // used to populate the caches, to ensure the responses are fresh.
    // If a URL is already hashed by Webpack, then there is no concern
    // about it being stale, and the cache-busting can be skipped.
    dontCacheBustUrlsMatching: /\.\w{8}\./,
    filename: 'service-worker.js',
    logger(message) {
      if (message.indexOf('Total precache size is') === 0) {
        // This message occurs for every build and is a bit too noisy.
        return;
      }

      if (message.indexOf('Skipping static resource') === 0) {
        // This message obscures real errors so we ignore it.
        // https://github.com/facebook/create-react-app/issues/2612
        return;
      }

      console.log(message);
    },
    minify: true,
    // Don't precache sourcemaps (they're large) and build asset manifest:
    staticFileGlobsIgnorePatterns: [/\.map$/, /asset-manifest\.json$/],
    // `navigateFallback` and `navigateFallbackWhitelist` are disabled by default; see
    // https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/template/README.md#service-worker-considerations
    navigateFallback: PUBLIC_PATH + '/index.html',
    navigateFallbackWhitelist: [/^(?!\/__).*/],
  }),

  !PRODUCTION
    ? new HotModuleReplacementPlugin()
    : void(0),
].filter(Boolean);


/* webpack.node */

const node = {
  dgram: 'empty',
  fs: 'empty',
  net: 'empty',
  tls: 'empty',
  child_process: 'empty',
};


/* webpack.optimization */

const minimizer = [
  new UglifyJsPlugin({
    uglifyOptions: {
      parse: {
        // we want uglify-js to parse ecma 8 code. However, we don't want it
        // to apply any minfication steps that turns valid ecma 5 code
        // into invalid ecma 5 code. This is why the 'compress' and 'output'
        // sections only apply transformations that are ecma 5 safe
        // https://github.com/facebook/create-react-app/pull/4234
        ecma: 8,
      },
      compress: {
        ecma: 5,
        warnings: false,
        // Disabled because of an issue with Uglify breaking seemingly valid code:
        // https://github.com/facebook/create-react-app/issues/2376
        // Pending further investigation:
        // https://github.com/mishoo/UglifyJS2/issues/2011
        comparisons: false,
      },
      mangle: {
        safari10: true,
      },
      output: {
        ecma: 5,
        comments: false,
        // Turned on because emoji and regex is not minified properly using default
        // https://github.com/facebook/create-react-app/issues/2488
        ascii_only: true,
      },
    },
    // Use multi-process parallel running to improve the build speed
    // Default number of concurrent runs: os.cpus().length - 1
    parallel: true,
    // Enable file caching
    cache: true,
    sourceMap: true,
  }),

  new OptimizeCSSAssetsPlugin(),
];

const optimization = {
  minimizer: PRODUCTION
    ? minimizer
    : void(0),
  // Automatically split vendor and commons
  // https://twitter.com/wSokra/status/969633336732905474
  // https://medium.com/webpack/webpack-4-code-splitting-chunk-graph-and-the-splitchunks-optimization-be739a861366
  splitChunks: {
    chunks: 'all',
    name: 'vendors',
  },
  // Keep the runtime chunk seperated to enable long term caching
  // https://twitter.com/wSokra/status/969679223278505985
  runtimeChunk: true,
};


/* Exports */

module.exports = {
  mode: MODE,
  entry,
  output,
  devtool,
  devServer: MODE === 'development'
    ? server
    : void(0),
  module: modules,
  resolve,
  plugins,
  optimization,
  node,
};
