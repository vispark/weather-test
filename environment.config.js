const path = require('path');

require('dotenv')
  .config({path: path.resolve(__dirname, '.env')});

strict(process.env, [
  'NODE_ENV',
  'PROTOCOL',
  'HOST',
  'PORT',
  'OPENWEATHERMAP_API_KEY',
]);

const {
  NODE_ENV,
  PROTOCOL,
  HOST,
  PORT,
  OPENWEATHERMAP_API_KEY,
} = process.env;

const MODE = NODE_ENV || 'development';
const PRODUCTION = MODE === 'production';
const PUBLIC_PATH = `${PROTOCOL}://${HOST}${Boolean(PORT) ? `:${PORT}` : ''}`;

module.exports = {
  MODE,
  PRODUCTION,
  HOST,
  PORT,
  PUBLIC_PATH,
  OPENWEATHERMAP_API_KEY,
};

/* Utilities */

function strict(object, required) {
  for (const field of required) {
    if (!object[field]) {
      throw new Error(`[${field}] is required.`);
    }
  }
}
