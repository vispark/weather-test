import React from 'react';

import {Container} from './index.styled';

export default ({children}) => (
  <Container>
    {children}
  </Container>
);
