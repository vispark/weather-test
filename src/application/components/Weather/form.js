import React, {Component} from 'react';
import PropTypes from 'prop-types';

export default class extends Component {
  constructor(props) {
    super(props);

    this.location = '';
  }

  static propTypes = {
    handler: PropTypes.func.isRequired,
  };

  render() {
    const {handler} = this.props;

    return (
      <>
        <input type="text" placeholder='Your location' onChange={(e) => this.location = e.target.value}/>
        <button onClick={() => handler(this.location)}>Show weather</button>
      </>
    );
  }
}