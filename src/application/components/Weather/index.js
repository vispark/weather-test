import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

import {weather} from 'Core/weather/redux/actions';

import Form from './form';
import Card from './card';

class Weather extends Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    current: PropTypes.shape(),
    forecast: PropTypes.array,
    isError: PropTypes.bool,
    load: PropTypes.func.isRequired,
  };

  handler(location) {
    this.props.load(location);
  }

  render() {
    const {
      isError,
      current,
      forecast,
    } = this.props;

    return (
      <>
        <Form handler={(location) => this.handler(location)}/>

        {isError && <div>Something went wrong. Please try another location</div>}

        {current && forecast && !isError &&
          <>
            <h3>Current weather:</h3>
            <Card weather={current}/>

            <h3>Forecast:</h3>

            {forecast.map((item, index) =>
              <Card key={index} weather={item}/>)}
          </>
        }
      </>
    );
  }
}

export default connect(
  ({weather: {current, forecast, isError}}) => ({current, forecast, isError}),
  dispatch => ({
    load: (location) => dispatch(weather.request(location)),
  }),
)(Weather);
