import React from 'react';
import PropTypes from 'prop-types';

import {CardStyled} from './card.styled';

function Card(props) {
  const {
    date,
    temp,
    temp_min,
    temp_max,
    description,
  } = props.weather;

  return (
    <CardStyled>
      {date && <div>Date: {date}</div>}
      <div>Description: {description}</div>
      <div>Temperature: {temp} K</div>
      {temp_min && <div>Min temperature: {temp_min} K</div>}
      {temp_max && <div>Max temperature: {temp_max} K</div>}
    </CardStyled>
  );
}

Card.propTypes = {
  weather: PropTypes.shape({
    date: PropTypes.string,
    temp: PropTypes.number.isRequired,
    temp_min: PropTypes.number,
    temp_max: PropTypes.number,
    description: PropTypes.string.isRequired,
  }).isRequired,
};

export default Card;
