import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';

import store from 'Core/store';

import App from './App';

const ROOT_ELEMENT = document.querySelector('#root');

function initiate(Component) {
  return () =>
    render(
      <Provider store={store}>
        <Component />
      </Provider>,

      ROOT_ELEMENT,
    );
}

if (module.hot) {
  module
    .hot
    .accept('./App.js', initiate(App));
}

initiate(App)();
