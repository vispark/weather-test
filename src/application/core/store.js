import {createStore, applyMiddleware} from 'redux';
import createSagaMiddleware from 'redux-saga';

import logger from 'Core/utils/logger';

import sagas from './sagas';
import reducers from './reducers';

const initial = {};

const saga = createSagaMiddleware();
const middleware = [logger, saga];
const store = createStore(reducers, initial, applyMiddleware(...middleware));

saga.run(sagas);

if (module.hot) {
  module.hot
    .accept('./reducers', () =>
      store.replaceReducer(require('./reducers').default));
}

export default store;
