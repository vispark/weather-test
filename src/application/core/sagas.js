import {all} from 'redux-saga/effects';

import weather from 'Core/weather/redux/sagas';

export default function* sagas() {
  yield all([
    ...weather,
  ]);
}
