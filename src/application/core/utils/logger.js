function colour(type) {
  switch (true) {
    case (/\/failed$/).test(type): {
      return '#F4511E';
    }

    case (/\/succeeded$/).test(type): {
      return '#43A047';
    }

    default: {
      return '#00ACC1';
    }
  }
}

/**
 * Logs all actions and states after they are dispatched.
 */
export default store => next => action => {
  const styles = `font-weight: bold; color: ${colour(action.type)};`;
  const result = next(action);
  const state = store.getState();

  console.group(`%c${action.type}`, styles);
  console.info('dispatching', action);
  console.log('next state', state);
  console.groupEnd();

  return result;
};
