export const get = method('GET');
export const put = method('PUT');
export const post = method('POST');
export const patch = method('PATCH');
export const remove = method('DELETE');

export default {
  get,
  put,
  post,
  patch,
  remove,
};

function method(method) {
  return async function _request(urn = '', body) {
    return await fetch(urn, options({method, body}))
      .then(response =>
        response.json());
  };
}

function headers(multipart) {
  const headers = {
    'Accept': 'application/json',
  };

  if (!multipart) {
    headers['Content-Type'] = 'application/json';
  }

  return headers;
}

function options(params = {}) {
  const {body} = params;
  const multipart = body instanceof FormData;

  return {
    // mode: 'cors',
    // cache: 'default',
    // headers: headers(multipart),
    ...params,
    body: multipart
      ? body
      : JSON.stringify(body),
  };
}
