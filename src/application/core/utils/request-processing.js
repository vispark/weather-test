import {put, call} from 'redux-saga/effects';

export function generator(request, ...params) {
  return function* _make({failed, succeeded}) {
    try {
      const {status, data, error} = yield call(request, ...params);

      if (error || status >= 300) {
        yield put(failed({status, error}));

        return void(0);
      }

      yield put(succeeded(data));

      return data;
    } catch(error) {
      yield put(failed({status, error}));
    }
  };
}

export default {
  generator,
};
