import {combineReducers} from 'redux';

import weather from 'Core/weather/redux/reducer';

export default combineReducers({
  weather,
});
