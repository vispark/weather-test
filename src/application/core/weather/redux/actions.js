export const WEATHER = {};

WEATHER.REQUESTED = 'weather/requested';
WEATHER.FAILED = 'weather/failed';
WEATHER.SUCCEEDED = 'weather/succeeded';

export const weather = {
  request: (location) =>
    ({type: WEATHER.REQUESTED, payload: location}),

  failed: error =>
    ({type: WEATHER.FAILED, payload: error}),

  succeeded: (payload) =>
    ({type: WEATHER.SUCCEEDED, payload}),
};
