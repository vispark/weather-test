import {
  put,
  fork,
  take,
  actionChannel,
} from 'redux-saga/effects';

import request from 'Core/utils/request-processing';

import {
  WEATHER,
  weather,
} from './actions';

import {getCurrentWeatherAndForecast} from '../endpoints';

function* load() {
  const channel = yield actionChannel(WEATHER.REQUESTED);

  while (true) {
    try {
      const {payload} = yield take(channel);
      yield request.generator(getCurrentWeatherAndForecast, {location: payload})(weather);
    } catch (error) {
      yield put(weather.failed(error));
    }
  }
}

export default [
  fork(load),
];
