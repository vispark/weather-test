import {WEATHER} from './actions';

const initial = {
  isError: false,
};

export default (state = initial, {type, payload}) => {
  switch (type) {

    case WEATHER.SUCCEEDED: {
      const {current, forecast} = payload;
      return {
        current,
        forecast,
        isError: false,
      };
    }

    case WEATHER.FAILED: {
      return {
        isError: true,
      }
    }

    default: {
      return state;
    }
  }
}
