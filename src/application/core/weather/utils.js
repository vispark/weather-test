export const symplify = (obj) => ({
  date: obj.dt_txt,
  temp: obj.main.temp,
  temp_max: obj.main.temp_max,
  temp_min: obj.main.temp_min,
  description: obj.weather[0].description,
});
