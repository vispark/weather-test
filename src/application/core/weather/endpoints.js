import {get} from 'Core/utils/request';
import {OPENWEATHERMAP_API_KEY} from 'Core/constants';

import {symplify} from './utils';

const API_URL = 'http://api.openweathermap.org/data/2.5';

const getCurrentWeather = (location) =>
  get(`${API_URL}/weather?q=${location}&APPID=${OPENWEATHERMAP_API_KEY}`);

const getForecast = (location) =>
  get(`${API_URL}/forecast?q=${location}&APPID=${OPENWEATHERMAP_API_KEY}`);

export const getCurrentWeatherAndForecast = ({location}) => {
  return Promise.all([
    getCurrentWeather(location),
    getForecast(location),
  ])
    .then(values => ({
      data: {
        current: symplify(values[0]),
        forecast: values[1].list.map(symplify),
      },
    }));
};
