import React from 'react';

import Container from 'Components/Container';
import Header from 'Components/Header';
import Weather from 'Components/Weather';

export default () => (
  <Container>
    <Header/>
    <Weather/>
  </Container>
);
